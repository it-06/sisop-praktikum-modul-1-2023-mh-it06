#!/bin/bash

read -p "Masukkan email: " email
read -p "Masukkan password: " password
echo

encrypted_password=$(echo -n "$password" | base64)

if ! grep -q "$email" users/users.txt; then
    echo "LOGIN FAILED - Email $email is not registered, please register first"

    log_date=$(date "+[%d/%m/%y %H:%M:%S]")
    echo "$log_date [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> users/auth.log

    exit 0
fi

if grep -q  " $encrypted_password" users/users.txt; then
username=$(grep " $encrypted_password" users/users.txt | awk '{print $2}')

    log_date=$(date "+[%d/%m/%y %H:%M:%S]")
    echo "$log_date [LOGIN SUCCESS] user $username logged in successfully" >> users/auth.log

    echo "LOGIN SUCCESS - Welcome, $username"
else
    echo "LOGIN FAILED - Incorrect password"

    log_date=$(date "+[%d/%m/%y %H:%M:%S]")
    echo "$log_date [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> users/auth.log
fi