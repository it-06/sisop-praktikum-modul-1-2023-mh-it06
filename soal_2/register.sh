#!/bin/bash


read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -p "Masukkan password: " password
echo


if [ ${#password} -le 8 ]; then
    echo "REGISTER FAILED - Password harus lebih dari 8 karakter"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi


if ! [[ "$password" =~ [A-Z] ]]; then
    echo "REGISTER FAILED - Password harus mengandung setidaknya 1 huruf kapital"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi


if [ "$password" == "$username" ]; then
    echo "Password tidak boleh sama dengan username."
    exit 1
fi

if ! [[ "$password" =~ [a-z] ]]; then
    echo "REGISTER FAILED - Password harus mengandung setidaknya 1 huruf kecil"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi


if ! [[ "$password" =~ [0-9] ]]; then
    echo "REGISTER FAILED - Password harus mengandung setidaknya 1 angka"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi


if ! [[ "$password" =~ [!@\$%^*] ]]; then
    echo "REGISTER FAILED - Password harus mengandung setidaknya 1 simbol unik"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi

if grep -q "$email $username" users/users.txt; then
    echo "REGISTER FAILED - Email atau username sudah terdaftar"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with email [$email]" >> users/auth.log

    exit 0
fi

encrypted_password=$(echo -n "$password" | base64)

echo "$email $username $encrypted_password" >> users/users.txt

log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER SUCCESS] user $username registered successfully" >> users/auth.log

echo "REGISTER SUCCESS - Akun berhasil dibuat"