#!/bin/bash

target_path="/home/ubuntu/"

timestamp=$(date +"%Y%m%d%H%M%S")
log_file="/home/ubuntu/log/metrics_${timestamp}.log"
mem_metrics=$(free -m | awk 'NR==2 {
	line=$2 ","
	line=line $3 ","
	line=line $4 ","
	line=line $5 ","
	line=line $6 ","
	line=line $7
	print line}')

swap_metrics=$(free -m | awk 'NR==3 {
	line2=$2 ","
	line2=line2 $3 ","
	line2=line2 $4
	print line2}')

du_metrics=$(du -sh "/home/ubuntu/" | awk '{print $1}')

echo "$mem_metrics,$swap_metrics,$target_path,$du_metrics" > "$log_file"

chmod 700 "$log_file"
