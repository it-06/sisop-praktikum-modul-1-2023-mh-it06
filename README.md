# sisop-praktikum-modul 1-2023-MH-IT06

# Anggota

Mutiara Nurhaliza - 5027221010
Rafif Dhimaz Ardhana - 5027221066
Ahmad Fauzan Daniswara - 5027221057

## Daftar Isi

- [Soal 1](#soal-1)
  - [Deskripsi Soal 1](#deskripsi-soal-1)
  - [Penyelesaian Soal 1](#penyelesaian-soal-1)
  - [Output Soal 1](#Output-soal-1)
  - [Revisi Soal 1](#Output-soal-1)
  - [Kendala Soal 1](#Output-soal-1)
- [Soal 2](#soal-2)
  - [Deskripsi Soal 2](#deskripsi-soal-2)
  - [Penyelesaian Soal 2](#penyelesaian-soal-2)
  - [Output Soal 2](#Output-soal-2)
  - [Revisi Soal 2](#Output-soal-2)
  - [Kendala Soal 2](#Output-soal-2)


# Soal 1

## Deskripsi soal 

Farrel memiliki akses ke file playlist.csv yang berisi informasi tentang lagu-lagu populer di Spotify, termasuk genre, popularitas, tahun rilis, dan lainnya. Dia memiliki beberapa permintaan khusus, termasuk mencari lagu-lagu hip hop terpopuler, lagu-lagu dengan popularitas terendah yang dibuat oleh John Mayer, lagu-lagu populer pada tahun 2004, dan mencari lagu yang diciptakan oleh seseorang dengan nama pencipta "Ibu Sri".
## Penyelesaian soal

Menampilkan isi file playlist.csv, kemudian mengalirkan output dari cat playlist.csv ke perintah grep untuk mencari baris yang mengandung kata yang dicari dalam file "playlist.csv". Menggunakan perintah sort untuk mengurutkan baris dalam teks.

 -t',': Menggunakan koma sebagai pemisah kolom.

-k15: Menggunakan kolom ke-15 sebagai kolom kunci untuk pengurutan.

-n: Mengurutkan secara numerik (angka).

-r: Mengurutkan secara terbalik, sehingga baris dengan nilai tertinggi di kolom ke-15 muncul pertama.

Outputnya dialirkan menggunakan head untuk menampilkan sejumlah baris teratas dari input yang diberikan. Kemudian outputnya dipotong menggunakan cut agar hanya muncul judulnya saja.

```
cat playlist.csv | grep "hip hop" | sort -t',' -k15 -n -r | head -n 5 | cut -d',' -f2
cat playlist.csv | grep "John Mayer" | sort -t',' -k15 -n | head -n 5 | cut -d',' -f2
cat playlist.csv | grep ",2004," | sort -t',' -k15 -n -r | head -n 10 | cut -d',' -f2
cat playlist.csv | grep "Sri" | cut -d',' -f2

```


## Output

![Output](https://i.imgur.com/G1uYBVm.jpg)


## Revisi

Tidak ada

## Kendala

Tidak ada
# Soal 2

## Deskripsi soal
Shinichi harus membuat program register dan login.
Program register harus memeriksa keamanan password, email unik, dan persyaratan lainnya.
Program harus mengenkripsi password menggunakan base64.
Semua aktivitas register dan login harus dicatat dalam file auth.log.
Setelah login, program harus memberikan respons sesuai dengan hasil login.
Data register harus disimpan dalam file users.txt.

## Penyelesaian soal
Program register.sh memiliki beberapa fungsi yang dilakukan untuk memeriksa validitas informasi yang dimasukkan oleh pengguna dan menyimpan informasi akun yang berhasil dibuat.

#### Menerima input pengguna dan memasukkannya kedalam variabel

```
read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -p "Masukkan password: " password
echo

```

#### Validasi Panjang Password

Program memeriksa panjang password dengan menggunakkan if [ ${#password} -le 8 ].
Jika panjang password kurang dari atau sama dengan 8 karakter, program mencetak pesan "REGISTER FAILED - Password harus lebih dari 8 karakter" dan mencatat aktivitas gagal dalam auth.log.

```
if [ ${#password} -le 8 ]; then
    echo "REGISTER FAILED - Password harus lebih dari 8 karakter"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi
```
#### Validasi Huruf Kapital:

Program memeriksa apakah password mengandung setidaknya satu huruf kapital dengan menggunakan perintah if ! [[ "$password" =~ [A-Z] ]].
Jika tidak ada huruf kapital dalam password, program mencetak pesan "REGISTER FAILED - Password harus mengandung setidaknya 1 huruf kapital" dan mencatat aktivitas gagal dalam auth.log.

```
if ! [[ "$password" =~ [A-Z] ]]; then
    echo "REGISTER FAILED - Password harus mengandung setidaknya 1 huruf kapital"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi
```
#### Validasi Password Tidak Sama dengan Username:

Program memeriksa apakah password sama dengan username dengan menggunakan perintah if [ "$password" == "$username" ].
Jika password sama dengan username, program mencetak pesan "Password tidak boleh sama dengan username" dan menghentikan proses registrasi.

```
if [ "$password" == "$username" ]; then
    echo "Password tidak boleh sama dengan username."
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi
```
#### Validasi Huruf Kecil:

Program memeriksa apakah password mengandung setidaknya satu huruf kecil dengan menggunakan perintah if ! [[ "$password" =~ [a-z] ]].
Jika tidak ada huruf kecil dalam password, program mencetak pesan "REGISTER FAILED - Password harus mengandung setidaknya 1 huruf kecil" dan mencatat aktivitas gagal dalam auth.log.

```
if ! [[ "$password" =~ [a-z] ]]; then
    echo "REGISTER FAILED - Password harus mengandung setidaknya 1 huruf kecil"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi
```
#### Validasi Angka:

Program memeriksa apakah password mengandung setidaknya satu angka dengan menggunakan perintah if ! [[ "$password" =~ [0-9] ]].
Jika tidak ada angka dalam password, program mencetak pesan "REGISTER FAILED - Password harus mengandung setidaknya 1 angka" dan mencatat aktivitas gagal dalam auth.log.

```
if ! [[ "$password" =~ [0-9] ]]; then
    echo "REGISTER FAILED - Password harus mengandung setidaknya 1 angka"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi

```

#### Validasi Simbol Unik:

Program memeriksa apakah password mengandung setidaknya satu simbol unik (!, @, $, %, ^, atau *) dengan menggunakan perintah if ! [[ "$password" =~ [!@\$%^*] ]].
Jika tidak ada simbol unik dalam password, program mencetak pesan "REGISTER FAILED - Password harus mengandung setidaknya 1 simbol unik" dan mencatat aktivitas gagal dalam auth.log.

```
if ! [[ "$password" =~ [!@\$%^*] ]]; then
    echo "REGISTER FAILED - Password harus mengandung setidaknya 1 simbol unik"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with password" >> users/auth.log

    exit 0
fi

```
#### Validasi Unik Email dan Username:

Program memeriksa apakah email atau username yang dimasukkan sudah terdaftar dalam file users.txt dengan menggunakan perintah if grep -q "$email $username" users/users.txt.
Jika informasi sudah terdaftar, program mencetak pesan "REGISTER FAILED - Email atau username sudah terdaftar" dan mencatat aktivitas gagal dalam auth.log.

```
if grep -q "$email $username" users/users.txt; then
    echo "REGISTER FAILED - Email atau username sudah terdaftar"
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER FAILED] ERROR Failed register attempt on user with email [$email]" >> users/auth.log

    exit 0
fi

```
#### Enkripsi Password:

Password yang lolos semua validasi dienkripsi menggunakan base64

```
encrypted_password=$(echo -n "$password" | base64)

```
#### Simpan Informasi Akun:

Informasi akun yang berhasil diregistrasi (email, username, dan password terenkripsi) disimpan dalam file users.txt 

```
echo "$email $username $encrypted_password" >> users/users.txt

```
#### Catat Aktivitas Registrasi:

Aktivitas registrasi yang berhasil atau gagal dicatat dalam file auth.log bersama dengan timestamp (waktu) dan jenis aktivitas.

```
log_date=$(date "+[%d/%m/%y %H:%M:%S]")
echo "$log_date [REGISTER SUCCESS] user $username registered successfully" >> users/auth.log

```
#### Cetak Pesan Respon:

Akhirnya, program mencetak pesan respon yang sesuai dengan hasil registrasi, baik berhasil maupun gagal.

```
echo "REGISTER SUCCESS - Akun berhasil dibuat"

```
## Output

![App Screenshot](https://i.imgur.com/cj8IkVN.jpg)

## Revisi

Tidak ada

## Kendala

Tidak ada
# Soal 3

## Deskripsi soal

Aether yang merupakan penggemar game Genshin Impact ingin mengoleksi foto-foto karakter dari player game tersebut dan Aether diberikan link drive kumpulan foto-foto karakter player game Genshin Impact oleh temannya yaitu Pierro. Namun setiap file yang ada di link dan sudah didownload tersebut terenkripsi oleh base64 dan Aether ingin mengembalikan nama file yang telah terenkripsi seperti sedia kala dengan membuat dua script yaitu genshin.sh dan find_me.sh. Script genshin.sh untuk merename setiap foto karakter dan memasukannya kedalam folder khusus, sedangkan script find_me.sh untuk menemukan endgame dari link yang diberikan oleh Pierro dengan ekstrak tiap gambar dan kemudian menyimpannya jika hasilnya sesuai harapan Aether.

## Penyelesaian soal

#### Unzip file genshin_character.zip

```
unzip genshin_character.zip

```

#### Merename foto dan decode base64

```
for encoded_file in *.jpg; do
    new_name=$(echo "$encoded_file" | base64 -d)
    mv "$encoded_file" "$new_name".jpg
    echo "Renamed $encoded_file to $new_name"
  done  

```

#### Membaca tiap data dari list_character.csv dan kemudian rename foto tadi dengan format yang baru

```
while IFS=, read -r Nama Region Element Senjata; do
     new_filename="$(echo "${Nama} - ${Region} - ${Element} - ${Senjata}.jpg" | tr -d '\r')"

```

#### Merename foto karakter Genshin Impact

```
mv "$file" "$new_filename"
            echo "Renamed $file to $new_filename"
            break  
        fi
    done
done < list_character.csv

```

#### Membuat folder berdasarkan region masing-masing karakter

```
mkdir -p Mondstat Inazuma Sumeru Liyue Fontaine

```

#### Loop File yang berformat.jpg lalu ekstrak region tiap karakter 

```
for file in *.jpg; do
region=$(echo "$file" | awk -F ' - ' '{print $2}')

```

#### Mengecek apakah folder region ada

```
if [ -d "$region" ]; then

```

#### Memindahkan foto ke masing masing region

```
mv "$file" "$region/"
        echo "Moved $file to $region/"
    else
       echo “Folder $region tidak ada. Lewati $file”
    fi
done

```

#### Hitung jumlah pengguna untuk tiap senjata

```
declare -A jumlah_senjata
for file in *.jpg
do
    senjata=$(echo "$file" | awk -F' - ' '{print $4}' | cut -d '.' -f 1)
    jumlah_senjata["$senjata"]=$((jumlah_senjata["$senjata"]+1))
done

```

#### Menampilkan jumlah pengguna untuk tiap senjata

```
for senjata in "${!jumlah_senjata[@]}"
do
    echo "$senjata : ${jumlah_senjata["$senjata"]}"
done


```

#### Hapus file yang tidak digunakan

```
rm genshin_character.zip list_character.csv genshin.zip

```

## Output 

#### Rename file foto dan decode base64

![App Screenshot](https://i.imgur.com/7ZN95ya.png)

#### Rename file foto dengan format baru(Nama-Region-Element-Senjata.jpg)

![App Screenshot](https://i.imgur.com/c1ugRbx.png)

#### Memindahkan file foto ke masing-masing region

![App Screenshot](https://i.imgur.com/gYX0c9i.png)

#### Menampilkan jumlah senjata dan mendelete file yang tidak diperlukan

![App Screenshot](https://i.imgur.com/eGzzIE1.png)


## Revisi

Revisi terletak pada kode saya yang berada di genshin.sh, Terdapat beberapa hal yang belum saya masukkan, yaitu bagian kode rename file foto dan decode base64, merename foto file dengan format terbaru, mengekstrak region tiap karakter, memindahkan file foto ke masing-masing region, serta pengerjaan script find_me.sh yang belum terlaksana.

## Kendala

Kendala terdapat pada bagian menampilkan jumlah senjata, saya masih belum tau code/command yang cocok untuk bisa menampilkan output jumlah senjata.

# Soal 4

## Deskripsi soal

Defatra akan membuat sebuah resource monitoring program yang dapat berjalan setiap menitnya secara otomatis. Parametric yang ingin dimonitor pada program tersebut adalah ram dan juga size dari suatu directory. Data dari metrics yang telah didapatkan akan disimpan ke sebuah log file yang hanya bisa dilihat oleh user yang memiliki/membuat file tersebut. Lalu dari log file yang telah didapatkan, akan diproses lagi sehingga terdapat log file baru yang berjalan secara otomatis setiap jam untuk menampilkan rata-rata, maksimum, dan juga minimum dari sejumlah data log file yang dibuat setiap menitnya.

## Penyelesaian soal

Akan dibuat dua file script (.sh) yang akan menjalankan tugasnya masing-masing. Pertama ada script minutes_log.sh yang akan menghasilkan log file setiap menitnya. Kedua ada script aggregate_mintues_to_hourly_log.sh yang berfungsi untuk melakukan agregasi file yang dihasilkan oleh script minute_log.sh menjadi satu file log yang akan dihasilkan setiap jamnya dengan isi nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics pada output log file script tersebut.

#### Script minute_log.sh: 

Menambahkan variabel untuk menampilkan timestamp dengan format “YYYYMMDDHHmmss” (dijadikan variabel agar dapat dengan mudah di print/echo). Serta mendeklarasikan target path dan juga target output file beserta format namanya. 

```
timestamp=$(date +"%Y%m%d%H%M%S")
target_path="/home/ubuntu/"
log_file="/home/ubuntu/log/metrics_${timestamp}.log"

```
Lalu dari command ‘free -m’, hanya diambil beberapa data yang diperlukan saja dengan menggunakan command awk. Command awk dilanjutkan dengan ‘NR==X’ dimana ‘NR’ adalah command untuk memanipulasi data hanya dalam satu baris, dan ‘X’ adalah nilai baris yang ingin dimanipulasi datanya. Command awk digunakan dua kali, masing-masing dinyatakan dalam variabel yang berbeda karena ada dua baris yang ingin dimanipulasi datanya.

```
mem_metrics=$(free -m | awk 'NR==2 {
	line=$2 ","
	line=line $3 ","
 	line=line $4 ","
 	line=line $5 ","
 	line=line $6 ","
 	line=line $7
 	print line}')
 	
swap_metrics=$(free -m | awk 'NR==3 {
 	line2=$2 ","  
 	line2=line2 $3 ","
 	line2=line2 $4
 	print line2}')

```
Command ‘du -sh’ dijadikan variabel juga agar dapat diambil salah satu outputnya saja, yakni size dari direktorinya dengan menggunakan command awk juga.

```
du_metrics=$(du -sh "/home/ubuntu/" | awk '{print $1}')

```

Setelah command tersebut dijadikan variabel semua dan sesuai dengan ketentuan outputnya, maka dijalankan fungsi echo untuk menghasilkan output script kedalam sebuah file baru (dalam kasus ini log file).

```
echo "$mem_metrics,$swap_metrics,$target_path,$du_metrics" >> "$log_file"

```
Terdapat juga command chmod 700 yang digunakan agar file tersebut hanya bisa diakses oleh user pemilik/pembuat file.

```
chmod 700 "$log_file"

```
## Output

![App Screenshot](https://i.imgur.com/haecPR6.png)

## Revisi

Karena pada saat demo kelompok kami belum selesai untuk pengerjaan nomor 4 bagian C, maka dalam bagian revisi saya tambahkan script untuk penyelesaian nomor tersebut. Pada nomor 4 bagian C diminta untuk membuat script baru yang diberi nama aggregate_minutes_to_hourly_log.sh. Fungsi dari script tersebut adalah untuk melakukan agregasi hasil output script minute_log.sh dan mengeluarkan output log baru yang didalamnya terdapat nilai minimum, maximum, dan rata-rata dari file-file log hasil minute_log.sh. Code dari script tersebut adalah seperti demikian:

```
#!/bin/bash

log_dir="/home/ubuntu/log"
timestamp=$(date +"%Y%m%d%H")
agg_log_file="/home/ubuntu/aggregated_log/metrics_agg_${timestamp}.log"

min_ram=9999999
max_ram=0
total_ram=0
min_swap=9999999
max_swap=0
total_swap=0
min_path_size=9999999
max_path_size=0
total_path_size=0
count=0
  
for log_file in $(ls $log_dir/metrics_*.log); do
if [ -f "$log_file" ]; then
metrics_line=$(tail -n 1 "$log_file")
    
mem_total=$(echo "$metrics_line" | awk -F ',' '{print $1}')
mem_used=$(echo "$metrics_line" | awk -F ',' '{print $2}')
mem_free=$(echo "$metrics_line" | awk -F ',' '{print $3}')
swap_total=$(echo "$metrics_line" | awk -F ',' '{print $7}')
swap_used=$(echo "$metrics_line" | awk -F ',' '{print $8}')
path_size=$(echo "$metrics_line" | awk -F ',' '{print $11}' | sed 's/M//')
     
if [ "$mem_total" -lt "$min_ram" ]; then
min_ram="$mem_total"
fi
if [ "$mem_total" -gt "$max_ram" ]; then
max_ram="$mem_total"
fi
total_ram=$((total_ram + mem_total))
     
if [ "$swap_total" -lt "$min_swap" ]; then
min_swap="$swap_total"
fi
if [ "$swap_total" -gt "$max_swap" ]; then
max_swap="$swap_total"
fi
total_swap=$((total_swap + swap_total))
    
if [ "$path_size" -lt "$min_path_size" ]; then
min_path_size="$path_size"
fi
if [ "$path_size" -gt "$max_path_size" ]; then
max_path_size="$path_size"
fi
total_path_size=$((total_path_size + path_size))
   
count=$((count + 1))
fi
done

if [ "$count" -gt 0 ]; then
avg_ram=$((total_ram / count))
avg_swap=$((total_swap / count))
avg_path_size=$((total_path_size / count))
else
avg_ram=0
avg_swap=0
avg_path_size=0
fi

echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_swap,$min_swap,$min_swap,$min_path_size,$min_path_size" >> "$agg_log_file"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_swap,$max_swap,$max_swap,$max_path_size,$max_path_size" >> "$agg_log_file"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_swap,$avg_swap,$avg_swap,$avg_path_size,$avg_path_size" >> "$agg_log_file"
 
chmod 700 "$agg_log_file"

#Command crontab yang digunakan untuk melakukan otomasi script setiap satu jam adalah:
#0 * * * * /home/ubuntu/praktiksisop/modul1/aggregate_minutes_to_hourly_log.sh

```
Dalam script ini, harus dideklarasikan terlebih dahulu nilai dari variabel yang akan digunakan untuk memanipulasi data dari output script minute_log.sh. Dari variabel-variabel yang telah dideklarasikan ada satu yang mencolok dan berbeda, yakni variabel count. Variabel ini digunakan untuk menghitung berapa banyak file log yang telah diproses agar nantinya bisa digunakan dalam menghitung rata-ratanya.

```
min_ram=9999999
max_ram=0
total_ram=0
min_swap=9999999
max_swap=0
total_swap=0
min_path_size=9999999
max_path_size=0
total_path_size=0
count=0

```
Script kemudian masuk kedalam status loop untuk memproses setiap file log yang diambil dari target directory yang telah ditetapkan. Command dibawah digunakan untuk menampilkan/membuat list dengan format penamaan yang sama dalam target directory log. 

```
for log_file in $(ls $log_dir/metrics_*.log); do
```
Setelah itu, dengan command if-else program akan mengecek apakah file tersebut ada atau tidak. Jika iya, program akan mengekstrak baris terakhir dari file tersebut dan sekaligus membuat variabel baru untuk menyimpan hasil ekstrak tersebut.

```
if [ -f "$log_file" ]; then
metrics_line=$(tail -n 1 "$log_file")
```
Lalu, digunakannya command awk -F untuk menempatkan data yang telah diekstrak pada sebuah variabel agar dapat dimanipulasi dengan mudah. Khusus untuk ‘path_size’ ditambahkan command sed 's/M//' untuk mengambil nilai integernya saja dan menghiraukan karakter ‘M’ yang terdapat pada file log tersebut.
Kemudian program akan membandingkan nilai yang diekstrak ini dengan nilai minimum dan maksimum saat ini untuk setiap kategori (RAM, swap, dan ukuran jalur) dan memperbaruinya. Selain itu, ia menambahkan nilai saat ini ke total yang sedang berjalan untuk setiap kategori.
Setelah memproses semua file log, script akan menghitung nilai rata-rata (avg_ram, avg_swap, avg_path_size) dengan membagi nilai total (total_ram, total_swap, total_path_size) dengan hitungan (count).
Setelah semua proses itu selesai, program akan melakukan appending semua hasil operasinya kepada file log baru yang sudah dideklarasikan dengan variabel ‘agg_log_file’. Ditambahkan juga chmod 700 "$agg_log_file" agar file hanya bisa diakses oleh pemilik filenya. Selain itu agar script berjalan dengan otomatis setiap jamnya, maka ditambahkan command crontab seperti berikut:

```
0 * * * * /home/ubuntu/praktiksisop/modul1/aggregate_minutes_to_hourly_log.sh

```
## Output
![App Screenshot](https://i.imgur.com/A8Eajmt.png)

## Kendala
Output dari file log agregasi kurang sesuai dengan permintaan soal, namun logic penyelesaian soal kurang lebih sama dengan dipakai untuk menyelesaikan nilai maksimum, minimum, dan juga rata-rata untuk variabel lainnya.
