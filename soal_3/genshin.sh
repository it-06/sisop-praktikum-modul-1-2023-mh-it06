# Ekstrak file genshin_character.zip
unzip genshin_character.zip

# Decode nama file dengan base64 dan merename sesuai format yang diinginkan
while IFS=, read -r filename nama region element senjata
do
    nama_decode=$(echo "$filename" | base64 -d)
    file_baru="$nama - $region - $element - $senjata.jpg"
    mv "$decoded_name" "$file_baru"
done < list_character.csv

# Buat folder berdasarkan region dan Pindahkan file sesuai region
mkdir -p Mondstadt 
for file in *.jpg
do
    region=$(echo "$file" | awk -F' - ' '{print $2}')
    mv "$file" "$region"
done

# Hitung jumlah pengguna untuk tiap senjata
declare -A jumlah_senjata
for file in *.jpg
do
    senjata=$(echo "$file" | awk -F' - ' '{print $4}' | cut -d '.' -f 1)
    jumlah_senjata["$senjata"]=$((jumlah_senjata["$senjata"]+1))
done

# Menampilkan jumlah pengguna untuk tiap senjata
for senjata in "${!jumlah_senjata[@]}"
do
    echo "$senjata : ${jumlah_senjata["$senjata"]}"
done

# Hapus file yang tidak digunakan
rm genshin_character.zip list_character.csv genshin.zip
