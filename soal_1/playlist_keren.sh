#!/bin/bash

wget --no-check-certificate -O playlist.csv 'https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp'

echo "Tugas a: 5 Lagu Hip Hop Teratas berdasarkan Popularity:"
cat playlist.csv | grep "hip hop" | sort -t',' -k15 -n -r | head -n 5 | cut -d',' -f2

cat playlist.csv | grep "John Mayer" | sort -t',' -k15 -n | head -n 5 | cut -d',' -f2

echo "Tugas c: 10 Lagu Tahun 2004 dengan Popularity Tertinggi:"
cat playlist.csv | grep ",2004," | sort -t',' -k15 -n -r | head -n 10 | cut -d',' -f2

echo "Tugas d: Lagu dengan Pencipta Ibu Sri (Ganti dengan nama pencipta yang sesuai):"
cat playlist.csv | grep "Ibu Sri" | cut -d',' -f2